#!/bin/bash
mv /etc/yum.repos.d/CentOS-Media.repo /etc/yum.repos.d/CentOS-Media.repo.bak
cp /etc/yum.repos.d/CentOS-AppStream.repo /etc/yum.repos.d/CentOS-AppStream.repo.bak
cp /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak
cp /etc/yum.repos.d/CentOS-Extras.repo /etc/yum.repos.d/CentOS-Extras.repo.bak
cat > /etc/yum.repos.d/CentOS-Media.repo <<-EOF
[c8-media-BaseOS]
name=CentOS-BaseOS-\$releasever - Media
#baseurl=file:///media/CentOS/BaseOS
baseurl=file:///media/cdrom/BaseOS
#file:///media/cdrecorder/BaseOS
gpgcheck=0
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial

[c8-media-AppStream]
name=CentOS-AppStream-\$releasever - Media
#baseurl=file:///media/CentOS/AppStream
baseurl=file:///media/cdrom/AppStream
#file:///media/cdrecorder/AppStream
gpgcheck=0
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial
EOF
echo -e "[${green}Info${plain}] change  CentOS-Media.repo completed..."
cd /etc/yum.repos.d
sed -i 's:enabled=1:enabled=0:'  CentOS-AppStream.repo
sed -i 's:enabled=1:enabled=0:'  CentOS-Base.repo
sed -i 's:enabled=1:enabled=0:'  CentOS-Extras.repo
echo -e "[${green}Info${plain}] change  CentOS-AppStream.repo completed..."
echo -e "[${green}Info${plain}] change  CentOS-Base.repo completed..."
echo -e "[${green}Info${plain}] change  CentOS-Extras.repo completed..."
